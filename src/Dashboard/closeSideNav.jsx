import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Person from '../Assets/Images/person.png'
import DashboardIcon from '@material-ui/icons/Dashboard';
import ArtTrackIcon from '@material-ui/icons/ArtTrack';
import BallotIcon from '@material-ui/icons/Ballot';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import AssessmentIcon from '@material-ui/icons/Assessment';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';
import EmailIcon from '@material-ui/icons/Email';
import AllInboxIcon from '@material-ui/icons/AllInbox';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    padding:'0px 10px',
  },
}));


 const CloseSidenav = (props) => {
    const classes = useStyles();
  return(
    <div className={classes.root}>
    <div className='text-center my-5' style={{height:"150px"}}>
        <h5>DW</h5>
    </div>
    <Accordion>
      <AccordionSummary
     
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}> <DashboardIcon/></Typography>
      </AccordionSummary>
    </Accordion>

    <Accordion>
      <AccordionSummary
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><ArtTrackIcon/> </Typography>
      </AccordionSummary>

    </Accordion>

    <Accordion>
      <AccordionSummary
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><BallotIcon/></Typography>
      </AccordionSummary>
    </Accordion>

    <Accordion>
      <AccordionSummary
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><EmailIcon/> </Typography>
      </AccordionSummary>
    </Accordion>

    <Accordion>
      <AccordionSummary
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><AssignmentTurnedInIcon/></Typography>
      </AccordionSummary>
    </Accordion>

    <Accordion>
      <AccordionSummary
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><AssessmentIcon/></Typography>
      </AccordionSummary>
    
    </Accordion>

    <Accordion>
      <AccordionSummary
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><PermContactCalendarIcon/></Typography>
      </AccordionSummary>
    
    </Accordion>

    <Accordion>
      <AccordionSummary
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><AllInboxIcon/></Typography>
      </AccordionSummary>
    
    </Accordion>

  </div>
   )
  }

  export default CloseSidenav
