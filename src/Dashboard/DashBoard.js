import React,{useState,useEffect} from 'react'
import Sidenav from './sidenav'
import CloseSidenav from './closeSideNav';
import Header from './Header';
import DeleteIcon from '@material-ui/icons/Delete';
import ArtTrackIcon from '@material-ui/icons/ArtTrack';
import BallotIcon from '@material-ui/icons/Ballot';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import Checkbox from '@material-ui/core/Checkbox';
import EmailIcon from '@material-ui/icons/Email';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import Badge from '@material-ui/core/Badge';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(1),
    },
    extendedIcon: {
      marginRight: theme.spacing(1),
    },
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid lightgray',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
      },
  }));
  


const Dashboard = (props) => {
    const classes = useStyles();
    const [expand,setExpand] = useState(false)
    const [open, setOpen] = React.useState(false);
    const [email,setEmail] =  useState('');
    const [subject,setSubject] =  useState('');
    const [body,setBody] =  useState('');
    const [refresh,setRefresh] = useState([])
    const [userDetails,setUserDetails] = useState({})
    const [tabChnage,setTabChange] = useState('inbox')
    const [renderData,setRenderData] = useState([])
    const [deleteItems,setDeleteItems] = useState([])
    const[showemail,setShowemail] = useState(false)
    const[selectemail,setSelectemail]= useState({})
    const handleOpen = () => {
      setOpen(true);
    };
  
    const handleClose = () => {
      setOpen(false);
    };

    useEffect(()=>{
        let selfData = JSON.parse(localStorage.getItem("userData"))
        setUserDetails(selfData)
    },[refresh])
    useEffect(()=>{
        let selfData = JSON.parse(localStorage.getItem("userData"))
        if(tabChnage === 'inbox'){
            selfData?.emails?.map((data,index)=>{
                return data['id'] = `id_${index}`
            })
            setRenderData(selfData?.emails)
            setDeleteItems([])
        }else if('sentmail'){
            selfData?.sentmails?.map((data,index)=>{
                return data['id'] = `id_${index}`
            })
            setRenderData(selfData?.sentmails)
            setDeleteItems([])
        }
        setUserDetails(selfData)
    },[tabChnage])

    const sendMail = ()=>{
        const userData = JSON.parse(localStorage.getItem("allUsers"))
        if(email && subject &&body){
            let selectIndex = null
            let selfIndex = null
            let permission =  userData.filter((res,index)=>{
               if(res.email === email){
                selectIndex = index
                   return true
               }
             })
             let selfData = JSON.parse(localStorage.getItem("userData"))
           
             if(permission.length === 1){
                let data = {
                    toemail:email,
                    fromemail:selfData?.email,
                    subject:subject,
                    body:body,
                    time:new Date(),
                    check:false,
                }
                let selfpermission =  userData.filter((res,index)=>{
                    if(res.email === selfData.email){
                        selfIndex = index
                        return true
                    }
                  })
                let selfUser = selfpermission[0]
                selfUser.sentmails = [...selfUser.sentmails,data]
                 localStorage.setItem("userData",JSON.stringify(selfUser))
                 userData[selfIndex] = selfUser
                 localStorage.setItem("allUsers",JSON.stringify(userData))
                let sendUser = permission[0]
                sendUser.emails = [...sendUser.emails,data]
                userData[selectIndex] = sendUser
                localStorage.setItem("allUsers",JSON.stringify(userData))
                setOpen(false)
                setRefresh(!refresh)
                
             }else{
               alert("User Not Found")
             }
           }else{
             alert("Please Enter Required Fileds")
           }
    }

    const openMail = (data,userindex)=>{
      setSelectemail(data)
      setShowemail(true)
            const userData = JSON.parse(localStorage.getItem("allUsers"))
            let selfData = JSON.parse(localStorage.getItem("userData"))
            let selectIndex = null
            let permission =  userData.filter((res,index)=>{
               if(res.email === selfData.email){
                selectIndex = index
                   return true
               }
             })

             if(permission.length === 1){
                let sendUser = permission[0]
                sendUser.emails[userindex]['check'] = true
                userData[selectIndex] = sendUser
                localStorage.setItem("allUsers",JSON.stringify(userData))
                localStorage.setItem("userData",JSON.stringify(sendUser))
                setRefresh(!refresh)
             }
    }

    const deleteMails = (e,index)=>{
        if(e.target.checked){
            setDeleteItems([...deleteItems,renderData[index]])
        }else{
            let data = renderData[index]
            let filterData = deleteItems.filter((res)=> res.id !== data.id)
            setDeleteItems(filterData)
        }
    }

    const deleteselectedMails = ()=>{
        const userData = JSON.parse(localStorage.getItem("allUsers"))
        let selfData = JSON.parse(localStorage.getItem("userData"))
        let data = renderData
       
        for (let i = 0; i < data.length; i++) {
           for (let n = 0; n < deleteItems.length; n++) {
              if(data[i]['id'] === deleteItems[n]['id']){
                data.splice(i,1)
              }
           }
        }
        let selectIndex = null
        let permission =  userData.filter((res,index)=>{
           if(res.email === selfData.email){
            selectIndex = index
               return true
           }
         })

         if(permission.length === 1){
            let sendUser = permission[0]
            if(tabChnage==='inbox'){
                sendUser.emails  = data
            }else{
                sendUser.sentmails  = data
            }
         
            userData[selectIndex] = sendUser
            localStorage.setItem("allUsers",JSON.stringify(userData))
            localStorage.setItem("userData",JSON.stringify(sendUser))
            setRefresh(!refresh)
            window.location.reload()
         }
    }
  return(
    <div className='row p-0 w-100'>
        <div className={`${expand ? 'col-md-1' : 'col-md-2'} sidenav`}>
          {expand ? <CloseSidenav/> :  <Sidenav/>}
        </div>
        <div className={`${expand ? 'col-md-11' : 'col-md-10'} p-0`}>
        <Header setExpand={setExpand} expand={expand} refresh={refresh}/>
        <div className='row mailsBlock m-0 p-0'>
            <div className="col-md-3">
            <div className='p-2'>
                <button className='btn btn-success w-100' onClick={handleOpen}>Compose Mail</button>
                <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
              <div className='row'>
              <lable>To :</lable>   <input type="text" className="form-control" onChange={(e)=>setEmail(e.target.value)}/>
              <lable>Subject :</lable>   <input type="text" className="form-control" onChange={(e)=>setSubject(e.target.value)}/>
              <lable>Body :</lable>   <textarea className="form-control" onChange={(e)=>setBody(e.target.value)}>
              </textarea>
              </div>

              <div className='d-flex justify-content-center my-2 '>
                 <button className='btn btn-primary mx-3' onClick={sendMail}>SEND</button>
                 <button className='btn btn-primary' onClick={handleClose}>CANCEL</button>
              </div>
             
          </div>
        </Fade>
      </Modal>
                <h6 className='mt-3 mb-2'>FOLDERS</h6>
                <ul className='folders'>
                 <li onClick={()=>setTabChange("inbox")}><ArtTrackIcon fontSize='small' /><span>Inbox</span>  </li>
                 <li onClick={()=>setTabChange("sentmail")}><EmailIcon fontSize='small'/><span>Send Mail</span>({userDetails?.sentmails?.length})</li>
                 <li><BallotIcon fontSize='small'/><span>Important</span></li>
                 <li><AssignmentTurnedInIcon fontSize='small'/><span>Drafts</span></li>
                 <li><DeleteIcon fontSize='small'/><span>Trash</span></li>
                </ul>
            </div>
            </div>
            <div className='col-md-9'>
                <div className='inbox my-3 '>
                    <h3 className='px-4 py-2'>{tabChnage ==="inbox" ? 'Inbox' : 'SENT MAILS'}</h3>
                    <div className='px-4 py-2'>
                    <Button variant="outlined" size="small" color="primary" className={classes.margin}>
        <AutorenewIcon/>  Refresh
        </Button>
        
                    <IconButton aria-label="show 4 new mails" color="inherit">
            <Badge badgeContent={deleteItems.length} color="secondary">
              <DeleteIcon onClick={deleteselectedMails}/>
            </Badge>
          </IconButton>
                    </div>
                    <ul className='mailsList' >
                        {renderData.map((data,index)=>{
                            return <li onClick={()=>openMail(data,index)} className={`${(tabChnage === 'inbox' && data.check === false) ?'active':""}`} style={{cursor:'pointer'}}>
                                 <Checkbox
                                    color="primary"
                                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                                    onChange={(e)=>deleteMails(e,index)}
                                    defaultChecked={false}
                                />
                               <span>{tabChnage === 'inbox' ? data?.fromemail : data?.toemail}</span> 
                                <span className='mx-5'>{data?.subject} </span>
                            </li>
                          
                        })}
                    </ul>
                    <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={showemail}
        onClose={()=>setShowemail(false)}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={showemail}>
          <div className={classes.paper}>
              <div className='row'>
              <lable>Email :</lable>   <h3>{tabChnage === 'inbox' ? selectemail?.fromemail: selectemail?.toemail}</h3>
              <lable>Subject :</lable>  <h3>{selectemail?.subject}</h3> 
              <lable>Body :</lable>  <h5>{selectemail?.body}</h5>
              
              </div>

              <div className='d-flex justify-content-center my-2 '>
                 
                 <button className='btn btn-primary' onClick={()=>setShowemail(false)}>close</button>
              </div>
             
          </div>
        </Fade>
      </Modal>
                </div>
            </div>
        </div>
        </div>
    </div>
   )

 }

 export default Dashboard