import React, { useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Person from '../Assets/Images/person.png'
import DashboardIcon from '@material-ui/icons/Dashboard';
import ArtTrackIcon from '@material-ui/icons/ArtTrack';
import BallotIcon from '@material-ui/icons/Ballot';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';
import AssessmentIcon from '@material-ui/icons/Assessment';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';
import EmailIcon from '@material-ui/icons/Email';
import AllInboxIcon from '@material-ui/icons/AllInbox';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
    padding:'0px 10px',
  },
}));

 const Sidenav = (props) => {
    const classes = useStyles();
    const useData = JSON.parse(localStorage.getItem("userData"))
  return(
    <div className={classes.root}>
    <div className='text-center my-5'>
        <img src={Person} className="personimg"/>
        <h5>{useData.username}</h5>
        <h6>Art Direct</h6>
    </div>
    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}> <DashboardIcon/>{" "}Dashboards</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <ul>
           <li className='my-2'>Inbox</li>
           <li className='my-2'> Email view</li>
           <li className='my-2'>Compose Email</li>
           <li className='my-2'>Email templates</li>
       </ul>
      </AccordionDetails>
    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><ArtTrackIcon/> {" "}Layouts</Typography>
      </AccordionSummary>

    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><BallotIcon/> {" "}Graphs</Typography>
      </AccordionSummary>
    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><EmailIcon/> {" "}Mailbox</Typography>
      </AccordionSummary>
      <AccordionDetails>
       <ul>
           <li className='my-2'>Inbox</li>
           <li className='my-2'> Email view</li>
           <li className='my-2'>Compose Email</li>
           <li className='my-2'>Email templates</li>
       </ul>
      </AccordionDetails>
    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><AssignmentTurnedInIcon/>{' '} Metrics</Typography>
      </AccordionSummary>
    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><AssessmentIcon/>{' '} Widgets</Typography>
      </AccordionSummary>
    
    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><PermContactCalendarIcon/> {" "} Forms</Typography>
      </AccordionSummary>
    
    </Accordion>

    <Accordion>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
      >
        <Typography className={classes.heading}><AllInboxIcon/>{' '} App Views</Typography>
      </AccordionSummary>
    
    </Accordion>

  </div>
   )
  }

  export default Sidenav
