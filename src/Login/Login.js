import React,{useEffect, useState} from 'react'
import {Navigate} from 'react-router-dom'

const Login = () => {
    const [redirect,setRedirect] = useState(false)
    const [email,setEmail] = useState("")
    const [password,setPassword] = useState("")
    const [allUsers,setAllUsers] = useState([])
    const userData = [{
      email:"laitha@gmail.com",
      password:"12345",
      username:"lalitha",
      sentmails:[],
      emails:[],
    },
    {
      email:"ambika@gmail.com",
      password:"54321",
      username:"Ambika",
      sentmails:[],
      emails:[],
    }]
    const login = ()=>{
      //setRedirect(true)
      if(email && password){
      
       let permission =  allUsers.filter((res)=>{
          if(res.email === email && res.password === password){
              return true
          }
        })
        console.log(permission)
        if(permission.length === 1){
          localStorage.setItem("userData",JSON.stringify(permission[0]))
          setRedirect(true)
        }else{
          alert("Please Enter Valid Fileds")
        }
      }else{
        alert("Please Enter Required Fileds")
      }
    }
    useEffect(()=>{
      const data = JSON.parse(localStorage.getItem("allUsers"))
      if(!data){
        localStorage.setItem("allUsers",JSON.stringify(userData))
        setAllUsers(userData)
      }else{
        setAllUsers(data)
      }
    
    },[])

  return(
    <div className='login'>
       { redirect && <Navigate to="/dashboard"/> }
        <div className='LoginfieldBox d-flex justify-content-center'>
            <div className='my-4'>
            <h3>LOGIN</h3>
            <input type="text" className='form-control ' onChange={(e)=>setEmail(e.target.value)}/><br/>
            <input type="text" className='form-control' onChange={(e)=>setPassword(e.target.value)}/><br/>
            <button className='btn btn-primary' onClick={login}>Login</button>
            </div>
        </div>
    </div>
   )

 }

 export default Login